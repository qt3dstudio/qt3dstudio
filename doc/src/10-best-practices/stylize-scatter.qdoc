/****************************************************************************
**
** Copyright (C) 2020 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:FDL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Free Documentation License Usage
** Alternatively, this file may be used under the terms of the GNU Free
** Documentation License version 1.3 as published by the Free Software
** Foundation and appearing in the file included in the packaging of
** this file. Please review the following information to ensure
** the GNU Free Documentation License version 1.3 requirements
** will be met: https://www.gnu.org/licenses/fdl-1.3.html.
** $QT_END_LICENSE$
**
****************************************************************************/

/*!

\title Stylize Scatter
\page stylize-scatter-effect.html
\ingroup qt3dstudio-best-practices

The scatter effect scatters the layers pixels around their original
positions, giving it a smeared look.

\image effects-stylize-scatter.png

\section1 Properties

\table
  \header
    \li Property
    \li Description
  \row
    \li
      Scatter Amount
    \li
      Controls the scatter amount and displacement area size.
  \row
    \li
      Grain
    \li
      Controls the direction where the pixels are displaced.
  \row
    \li
      Randomize Every Frame
    \li
      If set to true, the displacement is calculated again for every frame.
\endtable

\section1 Usage



*/
